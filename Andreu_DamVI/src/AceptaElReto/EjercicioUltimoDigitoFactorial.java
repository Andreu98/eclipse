package AceptaElReto;

import java.util.Scanner;

public class EjercicioUltimoDigitoFactorial {

	public static void main(String[] args) {
		Scanner reader= new Scanner(System.in);

		/*  Tu primo Luis, de 12 a�os, est� aprendiendo a usar la calculadora. Su profesor le ha dicho que calcule el factorial de varios n�meros. Pero, para evitar que le tengan que copiar n�meros muy largos en el cuaderno, les ha pedido �nicamente el �ltimo d�gito, el de m�s a la derecha.

			Recordando que el factorial es la multiplicaci�n de todos los n�meros entre el n�mero y el uno (por ejemplo, el factorial de 8, escrito 8!, es 8 � 7 � 6 � 5 � 4 � 3 � 2 � 1), demuestra a tu primo Luis que t� eres capaz de hacerlo mucho m�s r�pido que �l. */
		
		int num=0;
		int factorial=1;
		
		System.out.print("inserta el numero a calcular el factorial");
				num=reader.nextInt();
		while (num !=0) {
			factorial = factorial* num;
			num--;
		}
		System.out.print("El factorial es "+factorial+".");
		
		
		
		
	}

}
