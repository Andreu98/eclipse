package Matrius;
import java.util.Scanner;

public class Chuches {
	
static final int MAX_FILAS=4;
static final int MAX_COLS=4;

/*M�quina de Llaminadures (chuches)

Implementar un programa per gestionar una m�quina expenedora de llaminadures de l�Ins. Sabadell. 
Cada llaminadura t� un nom i un preu propi. Podeu fer servir la seg�ent estructura i dades: 

Tamb� heu de pensar l�estructura per guardar l�estoc (unitats disponibles) que hi ha a la m�quina de cada llaminadura Inicialment 5 unitats per cada.
Hi haur� un men� amb les seg�ents opcions: 
Demanar llaminadura: demanar� la posici� (tecla) de la llaminadura que vol.  Per exemple, s� l�usuari demana la tecla 20, vol dir que est� demanant la llaminadura  de la filera 2, columna 0. De moment se suposa que l�usuari ha introdu�t les monedes oportunes. Si no hi ha unitats suficients s�avisar� a l�usuari. Si tot �s correcte. Es mostrar� missatge a l�usuari i es descomptar� estoc. 
Mostrar llaminadures: Es mostrar� el gr�fic en forma de matriu amb el nom de les llaminadures, identificant les fileres i columnes per facilitar a l�usuari el demanar-les. A les llaminadures que no hi hagi estoc, es mostrar� al costat un �*�. Important: noteu que la llargada del nom de cada llaminadura �s diferent, feu servir les funcions de String per tal que la pantalla sort� ben quadrada per pantalla. Si es possible feu que surti un la matriu dins d�un requadre. 
Reomplir llaminadures: Es tracta de demanar una llaminadura (com a l�apartat anterior) i introduir una quantitat per reomplir la m�quina. Per poder accedir a aquesta funci� es demanar� una contrasenya (�Chuches2018�)
Apagar m�quina: Sortir del programa indicant els diners recaptats. 
Amplicaci�:
Fer dues opcions de men� m�s: Una per introduir monedes i un altre per retornar monedes. A la primera, es mostrar� submen� per tal de introduir monedes de 1 ctms, 2 ctms, 5 ctms, 10 cmts, 20 cmts, 50 cmts i 1 �. Cal anar guardant els diners i mostrar-los per pantalla. 
Adaptar les altres opcions de men� per tal de no deixar demanar llaminadures si no hi ha prou diners i retornar canvi. 
L�opci� de retornar monedes, mostrar� missatge dels diners retornats i posar� el comptador de import introdu�t a zero. 
*/
	public static void main(String[] args) {
	Scanner reader = new Scanner(System.in);  
	Scanner reader2 = new Scanner(System.in); 
	String cosa;
	int num1, num2;
	int menu=0, longitud;
	String[] [] matrizchuches = {	{"KutKot", "Chicles Fresa", "Licositos","Palasos"},
									{"Kinder Malo", "Harrybo", "Chetaos", "Cosos"},
									{"Manzanas", "Estrella Damm", "Papa Roma", "Chicles Menta"},
									{"Licositos", "Crunchys", "Barraleche", "KutKot"}
		};
	
		
	
	double [][] precio= { {1.1, 0.8, 1.5, 0.9},
						  {1.8, 1,   1.2, 1  },
						  {0,5, 1.35, 1.2, 0.8},
						  {1.5, 1.1, 1.1, 1.1},
						  
		};
	
	int [][] stock = {	{5, 5, 5, 5},
						{5, 5, 5, 5},
						{5, 5, 5, 5},
						{5, 5, 5, 5},
					  
		};
	
		System.out.println("Bienvenido a Sabachuches");

	do {	
		
		System.out.println("Elige una opci�n ");
		         
	
		
		System.out.println("  ");
		System.out.println("1- Demanar llaminadura ");
		System.out.println("2- Mostrar llaminadures ");
		System.out.println("3- Reomplir llaminadures ");
		System.out.println("4- Apagar m�quina ");
		System.out.println("  ");
		System.out.println("  ");
		menu= reader.nextInt();
		switch (menu) {
			
			/* Demanar llaminadura: demanar� la posici� (tecla) de la llaminadura que vol.  
			 * Per exemple, s� l�usuari demana la tecla 20, vol dir que est� demanant la llaminadura  de la filera 2, columna 0. 
			 * De moment se suposa que l�usuari ha introdu�t les monedes oportunes.
			 *  Si no hi ha unitats suficients s�avisar� a l�usuari. Si tot �s correcte. 
			 *  Es mostrar� missatge a l�usuari i es descomptar� estoc. 
			 */
			case 1: { // Opcio 1 que demana llaminadura
				System.out.println("�Heu seleccionat l'opci� 1: Demanar llaminadura!");
				
				System.out.println(" ");
				System.out.println("Seleccioneu una llaminadura");
				
				cosa= reader2.nextLine();	
				
				String numero1 = cosa.substring(0,1);
				String numero2 = cosa.substring(1,2);
				
				 num1 =	Integer.parseInt(numero1);
				 num2 = Integer.parseInt(numero2);

			if (num1>3||num1<0||num2>3||num2<0)	{
			
				System.out.println("No existe ^^" );
			}
			else {
			if (stock [num1][num2]==0) {
				System.out.println("Stock no disponible :(");

			}
			else {
			System.out.println(matrizchuches [num1][num2]);	
			System.out.println("Disfrute de su "+ matrizchuches [num1][num2] );
			
			stock [num1][num2]= stock [num1][num2]-1; 
				
				}
			}	
				
				
				
			break;
				
			
			}
			case 2: {
				System.out.println("�Heu seleccionat l'opci� 2: Heu seleccionat l'opcio 2 Mostrar Llaminadures !");

				
				/*Mostrar llaminadures: Es mostrar� el gr�fic en forma de matriu amb el nom de les llaminadures, 
					identificant les fileres i columnes per facilitar a l�usuari el demanar-les. 
					A les llaminadures que no hi hagi estoc, es mostrar� al costat un �*�. 
					Important: noteu que la llargada del nom de cada llaminadura �s diferent, feu servir les 
					funcions de String per tal que la pantalla sort� ben quadrada per pantalla.
					Si es possible feu que surti un la matriu dins d�un requadre.*/
					
					
					
					//cada espacio de productos tiene que ser de 15 espacios
					String espacio = new String ("                                            ");
					System.out.println("         0              1              2               3");
					System.out.println("__________________________________________________________________");
					for (int f=0;f<4;f++) {
						
			        	for(int c=0;c<4;c++) {
			        		if (c==0) {
			        			System.out.print(f);
			        		}
			        		System.out.print("||");
			        	
			        		if (stock[f][c]>0) {
			        	
			        		matrizchuches[f][c]= matrizchuches [f][c].concat(espacio) ;
			        		matrizchuches[f][c]=matrizchuches[f][c].substring(0, 14);
			        		
			        		System.out.print(matrizchuches [f][c]);
			        		}
			        		
			        		if	(stock[f][c]==0) {
			        			matrizchuches[f][c]= matrizchuches [f][c].concat(espacio) ;
				        		matrizchuches[f][c]=matrizchuches[f][c].substring(0, 13)+'*';
				        		
				        		System.out.print(matrizchuches [f][c]);
			        		}
			        		
			        
			        		if (c==3) {
				        		System.out.print("||");
				        		
			        		}
			        	}
			        	System.out.println("");

			        	System.out.println("__________________________________________________________________");
			        	
			    	}	
			break;
			
			}
			case 3: {
				
			/* Reomplir llaminadures: Es tracta de demanar una llaminadura (com a l�apartat anterior) i
			 *  introduir una quantitat per reomplir la m�quina. Per poder accedir a aquesta funci� es demanar� una contrasenya (�Chuches2018�) 	
			 */
				int newchuches;
				Scanner contra = new Scanner(System.in);
				Scanner chuchesnuevas = new Scanner(System.in);
				String contrase�a; 
				
				System.out.println("Heu seleccionat l'opci� 3: Reomplir llaminadures");
				System.out.println(" ");
				System.out.println(" Introdueix la contrasenya :)");
				contrase�a = contra.nextLine();
		
				if  (contrase�a.equals("Chuches2018")) {
				
					System.out.println(" Benvingunt maestre:3");
					
					String espacio = new String ("                                            ");
					System.out.println("");
					System.out.println("__________________________________________________________________");
					for (int f=0;f<4;f++) {
						
			        	for(int c=0;c<4;c++) {
			        		System.out.print("||");
			        	
			        		if (stock[f][c]>0) {
			        	
			        		matrizchuches[f][c]= matrizchuches [f][c].concat(espacio) ;
			        		matrizchuches[f][c]=matrizchuches[f][c].substring(0, 14);
			        		
			        		System.out.print(matrizchuches [f][c]);
			        		}
			        		
			        		if	(stock[f][c]==0) {
			        			matrizchuches[f][c]= matrizchuches [f][c].concat(espacio) ;
				        		matrizchuches[f][c]=matrizchuches[f][c].substring(0, 13)+'*';
				        		
				        		System.out.print(matrizchuches [f][c]);
			        		}
			        		
			        		
			        		
			        		
			        		
			        		
			        		
			        		if (c==3) {
				        		System.out.print("||");
				        		
			        		}
			        	}
			        	System.out.println("");

			        	System.out.println("__________________________________________________________________");
			        	
			    	}	
					
		        	System.out.println("Selecciona la chuche que voleu repondre existencies");
				
		        	cosa= reader2.nextLine();	
					
					String numero1 = cosa.substring(0,1);
					String numero2 = cosa.substring(1,2);
					
					 num1 =	Integer.parseInt(numero1);
					 num2 = Integer.parseInt(numero2);

				if (num1>3||num1<0||num2>3||num2<0)	{
				
					System.out.println("No existe ^^" );
				}
				
				else {
					System.out.println("Heu seleccionat "+matrizchuches[num1][num2]);
					System.out.println(" ");
					System.out.println("�Cuantes unitats voleu posar?");
					newchuches= chuchesnuevas.nextInt();
					
					stock [num1][num2]= stock [num1][num2] + newchuches;
					}
				}
					else {
						System.out.println(" contrase�a incorrecta");
			}
			break;
			}
				
		
		
		
		
		} 
	}
	while (menu!=4);
	

	}
}
