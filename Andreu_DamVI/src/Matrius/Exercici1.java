package Matrius;
import java.util.Scanner;
public class Exercici1 {
	static final int MAX_FILES =4 ;
	static final int MAX_COLS= 3;
/* Declara una matriu 4x3 de nombres sencers i escriu els seus elements multiplicats per 5.*/

	public static void main(String[] args) {
		Scanner reader= new Scanner(System.in);
		int i, j; //indices para acceder a las casillas de la matriz
		//declaracion i creacion de una matriz de 4 filas y 3 columnas
		int [][] mat = new int [MAX_FILES] [MAX_COLS];
		
		//Llenamos la matriz
		for (i=0; i <MAX_FILES; i++) {
			for (j=0; j<MAX_COLS; j++) {
				System.out.print("Posa un valor enter a la posici� (" + i + "," + j + "): ");
				mat [i][j] = reader.nextInt();
				
			}
		}
	
	//tratamos el contenido de la matriz
		for (i=0; i<MAX_FILES; i++) {
			for (j=0; j<MAX_COLS; j++) {
				mat[i][j] = mat [i][j] *5; //multiplicamos los valores anteriores por 5
				System.out.print("("+mat[i][j]+")");
			}
			System.out.println();
			}
	
	reader.close();
	}
	

}
