package Matrius;
import java.util.Scanner;

public class Exercici2 {
	static final int MAX_FILES = 4;
	static final int MAX_COLS = 2;
	
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		int matriu1 [][] = new int[MAX_FILES][MAX_COLS];
		int matriu2 [][] = new int[MAX_FILES][MAX_COLS];
		int matriur [][] = new int[MAX_FILES][MAX_COLS];
		for (int f=0; f<MAX_FILES; f++) {
			for (int c=0; c<MAX_COLS; c++) {
				matriu1[f][c]= (int)( Math.random()*10);
				matriu2[f][c]= (int)( Math.random()*10);
				matriur[f][c]= matriu1[f][c]+matriu2[f][c];
			}
		}
		
		// Imprimir las tres matrices
		// Empezamos por las filas y para cada fila escribimos.
		// 1. Columnas matriz 1
		// 2. columnas matriz 2
		// 3. Columnas suma
		
		for (int f=0; f<MAX_FILES; f++) {
			//Imprimir"parentesis"
			System.out.println("");
			 System.out.print("| ");
			//Imprimir"columanas de la fila en curso de matriz 1"
			for (int c=0; c< MAX_COLS; c++) {
				 System.out.print(matriu1[f][c]+" ");
			}
			
			//Imprimir"parentesis"
			 System.out.print("|");
			
			//Imprimir"Espacio o el +"
			 if (f==1)System.out.print(" + ");
			 else System.out.print("   ");
			//Imprimir"parentesis"
			 System.out.print("| ");
			//Imprimir"columanas de la fila en curso de matriz 2"
			 for (int c=0; c< MAX_COLS; c++) {
				 System.out.print(matriu2[f][c]+" ");
			}
			//Imprimir"parentesis"
			 System.out.print("|");
			//Imprimir"Espacio o el ="
			 if (f==1)System.out.print(" = ");
			 else System.out.print("   ");
			//Imprimir"parentesis"
			 System.out.print("| ");
			//Imprimir"columanas de la fila en curso de resultado"
			 for (int c=0; c< MAX_COLS; c++) {
				 System.out.print(matriur[f][c]+" ");
			}
			//Imprimir"parentesis"
			 System.out.print("| ");
			
				
		}
		

	}
	
	

}
