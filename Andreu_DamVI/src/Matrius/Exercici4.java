package Matrius;
import java.util.Scanner;

public class Exercici4 {
	static final int MAX_FILAS=5;
	static final int MAX_COLS=10;
	public static void main(String[] args) {
		 Scanner reader = new Scanner(System.in);          
	        
			//Declaracio i creacio duna matriu de 4 files i 3 columnes        
		int [] [] matriz = new int [MAX_FILAS][MAX_COLS];
		int max=-1;  // Variable con el valor maximo en cada momento
		int fmax=0;
		int cmax=1;
		
		// Pedimos datos al usuario: Nosotros los generamos aleatoriamente
		
		for (int f=0 ; f <MAX_FILAS; f++) {
			for (int c=0 ; c<MAX_COLS; c++) {
				matriz[f][c]= (int) (Math.random()*20);				
			}
		}
		
		// REcorremos matriz en busca del valor maximo
		// Nlos lo apuntaremos en mx, y la posicion del maxiomo en fmax i cmax
		max=-1;  // Variable con el valor maximo en cada momento
		fmax=0;		
		cmax=0;		
		String tmpstr;
		for (int f=0 ; f <MAX_FILAS; f++) {
			System.out.println("");
			for (int c=0 ; c<MAX_COLS; c++) {
				// La vamos imprimiendo de paso
				tmpstr="    "+matriz[f][c]; // "126    " 
				System.out.print(    
						 tmpstr.substring(tmpstr.length()-3 ,  tmpstr.length()) 
						  );
				
				if (max<matriz[f][c]) {
					// Actualizamos nuevo valor maximo
					max=matriz[f][c];
					fmax = f;
					cmax = c;
					
				}				
			}
		}		
		
		
		
		System.out.println("");
		System.out.println("El valor maximo es "+max+" y esta en la fila "+
		                        fmax+" columna "+cmax);   
				  


	}

}
