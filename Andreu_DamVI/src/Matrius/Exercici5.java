package Matrius;
import java.util.Scanner;
public class Exercici5 {
/* 
 * Implementeu un programa que permeti fer la reserva de seients de la sala d�un cinema. La sala disposa de 20 fileres; a cada filera hi ha 15 seients.

El programa mostrar� un men� amb les seg�ents opcions:
Buidar sala: Prepara la sala per la seg�ent filmaci� de la pel�l�cula, marcant tots els seients lliures.
Visualitzar seients disponibles: El programa mostrar� una matriu corresponent als seients de la sala. Si el seient est� reservat es veur� un �*� i si el seient est� lliure es veur� un�_�
Reserva de seients: El programa demanar� les coordenades (fila i columna) del seient que vol reservar. Despr�s comprovar� que estigui lliure i si �s aix� la marcar� com a reservat. Per�, si est� ocupat, el programa proposar� el primer seient disponible que es trobi m�s a prop del mig. Si la filera estigu�s tota ocupada, avisar� de l�error.

NOTES: Cal controlar qualsevol tipus d�errada en les dades d�entrada. 
No es permetr� fer cap reserva ni visualitzar la sala si pr�viament no ha sigut buidada.


 */
	
	
	static final int MAX_FILAS=20;
	static final int MAX_COLS=15;
	
	public static void main(String[] args) {
		 Scanner reader = new Scanner(System.in);          
		 Scanner fila = new Scanner(System.in);          
		 Scanner columna = new Scanner(System.in);          
		 boolean flag;
		
		char  [][] MATRIZ =new char [MAX_FILAS][MAX_COLS];
		int MENU=0; 
		
	
		do {
			//MENU
			
			System.out.println("  ");
			System.out.println("1 Vaciar sala!");
			System.out.println("2 Visualizar sala");
			System.out.println("3 Reservar asientos");
			System.out.println("4 EXIT");
			System.out.println("   ");
		
			MENU = reader.nextInt();
			
			// SWITCH CON LAS OPCIONES DEL MENU
			
			
			
			
			
			
			
			
			switch (MENU) {
			
			
			//MENU 
			
			
		
			
			case 1: 
				//vaciamos el cine, es decir, llenamos a matriz-chan con '_'
				for (int f=0 ; f <MAX_FILAS; f++) {
					for (int c=0 ; c<MAX_COLS; c++) {
						MATRIZ[f][c]= '_' ;				
					}
					
				}
				
				System.out.println("Cine vaciado!");
				 
				break;
				
			case 2: 
				// con este glorioso bucle, hacemos el encabezado
			
				
					int t=0;
					while (t<15) {
					if (t==0) {System.out.print("    "+ 1 +"  "); 
					}
					if (t!=0&&t<9)
					System.out.print(t+1+"  ");
					if (t==9) System.out.print(t+1+" ");
					if (t>=10)
						System.out.print(t+1+ " " );
					t++;
					}
					
					
					
					System.out.println(" ");
			
					//Mostrem el contingut de la matriu	 
						for (int f=0;f<MAX_FILAS;f++) {
		        		if (f+1<10)
		        			//esta cosa es para que salga ordenado del 1 al 20
		        		System.out.print (f+1+"   ");
		        		else 
		        			System.out.print(f+1+"  "); 
		        		//
		        		for(int c=0;c<MAX_COLS;c++) {
		   	                	System.out.print(MATRIZ[f][c]+"  ");
		            	}
		            	System.out.println();
		        	}
					
					System.out.println(" ");
					System.out.println("�aqui tienes el cine!");
				
					break;
				
			case 3:
				
				System.out.println(" �Buenos dias! Reserve su localidad :) ");
				
				System.out.println(" �En que fila quiere reservar? del 1 al 20");

				// lo suyo seria guardar la posicion que queramos y cuando la fila sea = a f y la columna sea = a c pues se cambia por un *
				int filaescaneada;
				filaescaneada = fila.nextInt();
				
				//control de errores filas
				if (filaescaneada>20||filaescaneada<1) {
					flag=true;
					do {
						System.out.println(" �En que fila quiere reservar? del 1 al 20, acaso no sabes leer?");
						filaescaneada = fila.nextInt();
						if (filaescaneada>=1&&filaescaneada<=20) {
							flag=false;
					}
						
					}
					
					while (flag);
					
				}
			
				// fin control de errores filas
					
					
					
					
	
				
				System.out.println(" �En que columna quiere reservar? del 1 al 15");
				
				int columnaescaneada;
				columnaescaneada = columna.nextInt();
				
				//control de errores columnas
				if (columnaescaneada>15||columnaescaneada<1) {
					flag=true;
					do {
						System.out.println(" �En que fila quiere reservar? del 1 al 15, acaso no sabes leer?");
						columnaescaneada = fila.nextInt();
						if (columnaescaneada>=1&&columnaescaneada<=15) {
							flag=false;
					}
						
					}
					
					while (flag);
					
				}
			
				// fin control de errores columnas
				
				
				//y ahora cambiamos el valor de esa posicion de la matriz por un '*'
				

						MATRIZ[filaescaneada-1][columnaescaneada-1]= '*' ;				
					
				// Profe, no sabia como hacer el control de errores para hacer que al reservar , en una posicion ya reservada, se moviera hacia el centro.	
					
				
			
				
				
				
				
				
				
				
				break;
				
			default: 
				if (MENU!=4)
				System.out.println("APRENDE A LEER, NUMERO ENTRE 1 Y 4");
				else
					System.out.println("Adios");
			
			
			}
			
			
		} while (MENU!=4);
		
		

	}

}



