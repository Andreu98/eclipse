package Matrius;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Scanner;

public class Exercici7 {
/* Programa que omple un n�mero determinat de noms que demana a l�usuari.
 *  Despr�s demana per teclat un nom. El programa ha de comptar el n�mero de vegades que apareix aquest nom a la llista. */
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);  
		int contador=0;
		String nombre;
		ArrayList<String> nombres = new ArrayList<String>();
		
		while (contador!=7) {
		System.out.println("Mete 7 nombres a su gusto, llevas "+contador);
		
		nombre= reader.nextLine();
		
		nombres.add(nombre);
		
		contador++;
		}
		
		// System.out.println(nombres);
		System.out.println("Escribe un nombre y te digo si esta dentro, y cuantas veces aparece :)");
		nombre= reader.nextLine();
		
		if (nombres.contains(nombre)) {
			
			int frequencia = Collections.frequency(nombres, nombre);
			
			System.out.println("aparece "+frequencia+"veces");
		}
		else {
			
			System.out.println("No aparece ese nombre en el listado que pusiste antes, chico listo :*");
	
		}
		reader.close();
	}

}
