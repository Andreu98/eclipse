package Matrius;
import java.util.Scanner;
/* Un programa ens demana els elements d'una matriu 3x3 i ens escriu la matriu transposada i la suma de les dues matrius.*/
public class Opcional1 {
	
	static final int MAX_FILAS=3;
	static final int MAX_COLS=3;
	
	public static void main(String[] args) {
		int  [][] MATRIZ =new int [MAX_FILAS][MAX_COLS];
		int  [][] SumaTraspuesta =new int [MAX_FILAS][MAX_COLS];
		 Scanner reader = new Scanner(System.in);          

		for (int f=0 ; f <MAX_FILAS; f++) {
			for (int c=0 ; c<MAX_COLS; c++) {
				System.out.print("Mete un valor en la posicion de la matriz 3x3 ");
				System.out.print("["+f+",");
				System.out.print(" "+c+"]");
				MATRIZ[f][c]= reader.nextInt();		
			}
			
		}
		System.out.println(" ");
		System.out.println(" ");
		
		for (int f=0;f<MAX_FILAS;f++) {
        	for(int c=0;c<MAX_COLS;c++) {
	                System.out.print(MATRIZ[f][c]+" " );
        	}
        	System.out.println();
    	}
		
		System.out.println("    ");
		System.out.println("    ");
		//matriz traspuesta
		
		for (int f=0;f<MAX_FILAS;f++) {
        	for(int c=0;c<MAX_COLS;c++) {
	                System.out.print(MATRIZ[c][f]+" " );
        	}
        	System.out.println();
    	}
		
		//suma
    	System.out.println();

		for (int f=0;f<MAX_FILAS;f++) {
        	for(int c=0;c<MAX_COLS;c++) {
        		
        		if (MATRIZ[c][f]+MATRIZ[f][c]>=10) {
	                System.out.print(MATRIZ[c][f]+MATRIZ[f][c]+" " );
        				}

        		if (MATRIZ[c][f]+MATRIZ[f][c]<10) {
	                System.out.print(MATRIZ[c][f]+MATRIZ[f][c]+"  " );
        				}
        	
        	
        	
        	}
        	System.out.println();
    	}
		
		
		
		
		
		
		
		
	}

}
