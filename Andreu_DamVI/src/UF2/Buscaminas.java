package UF2;

import java.util.ArrayList;
import java.util.Scanner;

public class Buscaminas {
	static Scanner reader = new Scanner(System.in); // Variable global dins de la classe

	public static void main(String[] args) {
		int menuop = 0, lado = 8, minas = 0;
		ArrayList<String> Ganadores = new ArrayList<String>();
		int[][] tablero = new int[lado][lado];
		int[][] bombas = new int[lado][lado];
		String Usuario = null;
		boolean jugando = true;
		do {

			menuop = Menu();
			switch (menuop) {

			case 0: // salida
				System.out.println("Adios");
				break;

			case 1: // explicacion
				Ayuda();

				break;
			case 2: // opciones juego
				jugando = true;
				Usuario = Nombre();
				lado = Dimensiones();
				minas = NumeroMinas(lado);
				tablero = Matriztablero(lado);

				bombas = MatrizBombas(lado, minas);

				MostrarTableroMinas(bombas, lado);

				break;
			case 3: // Ranking

				System.out.println(Ganadores);
				break;
			case 4: // juego
				boolean fracasado=false;
				while (jugando == true) {
					int fila = 0, columna = 0, cercanas = 0,opcion=0;
					
					MostrarTableroUsuario(tablero, lado);
					
					System.out.println("deseas destapar o marcar mina?");
					System.out.println("0 PARA DESTAPAR,1 PARA MARCAR");
					opcion= reader.nextInt();
					System.out.print("Elige una fila (de 0 a ");
					System.out.print(lado - 1 + ")");
					fila = reader.nextInt();
					System.out.println();
					System.out.print("Elige una columna (de 0 a ");
					System.out.print(lado - 1 + ")");
					columna = reader.nextInt();
					System.out.println();
					
					if(opcion==0) {
					if (bombas[fila][columna] == 1) {
						jugando = false;
						fracasado=true;
						System.out.println("LOOSER");
					}
					if (bombas[fila][columna] == 0) {
												
						AbrirCasilla(lado,bombas,fila,columna,tablero);
						
					}

					}
					if(opcion==1) {
						MarcarMina(tablero,fila,columna);
					}
					
					jugando=ComprobarResultado(tablero,bombas,lado,minas);
				}
				if(fracasado==false&&jugando==false) {
					Ganadores.add(Usuario);
				}

				break;
			}

		}

		while (menuop != 0);

	}

	public static int Menu() // menu general
	{
		System.out.println(" 1. Mostrar Ajuda ");
		System.out.println(" 2. Opciones ");
		System.out.println(" 3. Visualizar ganadores  ");
		System.out.println(" 4. Jugar  ");
		System.out.println(" 0. Exit ");
		int k = reader.nextInt();
		while (k > 4 || k < 0) {
			System.out.println(" Error. Prueba otra vez ");
			System.out.println(" ");
			System.out.println(" 1. Mostrar Ajuda ");
			System.out.println(" 2. Opciones ");
			System.out.println(" 3. Visualizar ganadores  ");
			System.out.println(" 4. Jugar  ");
			System.out.println(" 0. Exit ");
			k = reader.nextInt();
		}
		return k; // devuelve el valor que elijas para el menu
	}

	public static void Ayuda() // ayuda menu general
	{
		System.out.println(" ");
		System.out.println(" ");
		System.out.println(" Seleccione 2 en el menu para determinar su nombre, el tama�o (f*c i el numero de minas");
		System.out.println("Si selecciona la opcion 3, veras el registro de personas que han ganado anteriormente");
		;
		System.out.println(" La opcion jugar permite jugar :)");
	}

	public static String Nombre() // nombre que mete el usuario
	{
		System.out.println(" ");
		System.out.println("Escribe tu nombre");
		String name = reader.next();
		return name;
	}

	public static int Dimensiones() // dimension del cuadrado del tablero
	{
		System.out.println("Introduce las dimensiones de tu tablero");
		int dimensiones = reader.nextInt();
		return dimensiones;
	}

	public static int NumeroMinas(int lado) // numero minas
	{

		int minas;
		do {
			System.out.println("Introduce el numero de minas");
			minas = reader.nextInt();

		} while ((minas >= ((lado * lado) - 1) || minas <= 0));

		return minas;
	}

	public static int[][] Matriztablero(int lado) // generamos la matriz tablero
	{
		int[][] matriz = new int[lado][lado];
		for (int f = 0; f < lado; f++) {
			for (int c = 0; c < lado; c++) {
				matriz[f][c] = 9;
			}

		}
		return matriz;
	}

	public static int[][] MatrizBombas(int lado, int minas) // generamos la matriz bombas
	{
		int[][] mbombas = new int[lado][lado];
		int fi, co;

		for (int f = 0; f < lado; f++) {
			for (int c = 0; c < lado; c++) {
				mbombas[f][c] = 0;
			}
		}

		do {
			fi = (int) (Math.random() * lado);
			co = (int) (Math.random() * lado);
			if (mbombas[fi][co] == 0) {
				mbombas[fi][co] = 1;
				minas--;
			}

		} while (minas != 0);

		// numero = (int) (Math.random() * n) + 1; Donde n es hasta el número que
		// quieres que llegue,

		return mbombas;
	}

	public static void MostrarTableroMinas(int[][] bombas, int lado) // mostramos la matriz bombas para comprovar que se
																		// genera correctamente
	{
		for (int f = 0; f < lado; f++) {
			for (int c = 0; c < lado; c++) {
				System.out.print("(" + bombas[f][c] + ")");
			}
			System.out.println();
		}
	}

	public static void MostrarTableroUsuario(int[][] tablero, int lado) {
		for (int f = 0; f < lado; f++) {
			for (int c = 0; c < lado; c++) {
				if (tablero[f][c]==10) {
				System.out.print("(" + tablero[f][c] + ")");
				}
				else {
				System.out.print("(" + tablero[f][c] + " )");
				}
			}
	       // return n*(factorial(n-1));
			System.out.println();
		}

	}
	
	public static int [][] AbrirCasilla(int lado, int [][] bombas, int fila, int columna, int [][]tablero) {
		
		int cercanas=0, XIni = fila - 1, XFin = fila + 1, YIni = columna - 1, Yfin = columna + 1;
	
		if (XIni < 0) 							XIni = 0;						
		if (XFin >= lado)						XFin = lado-1;						
		if (YIni < 0) 						YIni = 0;
		if (Yfin >= lado) 	Yfin = lado-1;

	//	System.out.println("x: "+XIni+" . "+XFin+" Y: "+YIni+"."+Yfin);
		for (int f = XIni; f <= XFin; f++) {
			for (int c = YIni; c <= Yfin; c++) {
				//System.out.println("x: "+f+" Y: "+c);
				if (bombas[f][c] == 1) {
				//	System.out.println("coso");
					cercanas++;
				}
			}
		}
		tablero[fila][columna] = cercanas;
		if (cercanas!=0) { 
			
			return tablero;
		}	else {			
			
			for (int f = XIni; f <= XFin; f++) {
				for (int c = YIni; c <= Yfin; c++) {
					if (!(f==fila && c==columna) && tablero[f][c]==9)		  tablero= AbrirCasilla(lado,bombas,f,c,tablero);

				}
			}
			return tablero;
		}
			
			
	}
	
	public static void MarcarMina(int [] []tablero,int fila, int lado) {
		
		if (tablero[fila][lado]==9) {
			tablero[fila][lado]=10;
		}
		else {
			if(tablero[fila][lado]==10) {
				tablero[fila][lado]=9;
			}
		}
	}
	
	public static boolean ComprobarResultado(int[][] tablero, int[][] bombas, int lado, int minas) {
		int contador=0;
		for (int f=0; f<lado; f++) {
			for(int c=0; c<lado; c++) {
				if(bombas[f][c]==1&&tablero[f][c]==10) {
					contador++;
				}
			}
		}
		if(contador==minas) {
			return false;
		}
		else return true;
	}


	
	
}