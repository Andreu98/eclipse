package UF2;

import java.util.Scanner;

/* Aplicant el disseny modular implementa un programa que realitzi les funcions d�una calculadora senzilla: suma, resta, multiplicaci�, divisi� entera i m�dul.
 *  La calculadora treballa �nicament amb nombres enters. Dispondr� d�un men� d�opcions:

Obtenir dos n�meros a operar
Sumar 
Restar
Multiplicar
Divisi�: Aqu� tornar� a demanar si es vol obtenir el m�dul o la divisi� entera
Visualitzar el resultat de l�operaci�.
Sortir del programa.

Comproveu possibles errades a les dades d�entrada.

Ara milloreu el programa perqu� el resultat de cada operaci� pugui ser acumulatiu, de forma que a l�opci� a,
 �nicament es demani dos n�meros quan no tinguem res acumulat. En cas contrari, demanar� un �nic valor. Afegiu l�opci� d�inicialitzar, posar a 0, el valor acumulat.
*/
public class Calculadora {
	
	static Scanner reader = new Scanner(System.in);  //Variable global dins de la classe
	
	public static void main(String[] args) {
		int opmenu,num1=0,num2 = 0, resultadogeneral=0;
		do {
		
			opmenu= PresentaMenu();
			switch (opmenu) {
			case 1 :
			//Obtener dos numeros
				
				System.out.println("Num1");
				num1=reader.nextInt();
			
				System.out.println("Num2");
				num2=reader.nextInt();
				
			  	break;
			case 2 :
				//Sumar
				resultadogeneral=Suma(num1, num2);
				
				break;
			case 3 :
				//Restar
				resultadogeneral=Resta(num1, num2);
				break;
			case 4 :
				//Multiplicar
				resultadogeneral=Multiplicar(num1,num2);
				break;
			case 5 :
				//Divisi�
				resultadogeneral= DivisionEntera(num1,num2);
				break;
			case 6 :
				//Visualitzar
				System.out.print(resultadogeneral);
				break;
			case 0 :
				System.out.println("Bye!");
				break;
			default :
				System.out.println("No has triat res");
		}
			
			
		}
		
		while(opmenu!=0);
		
		
	}

	 public static int PresentaMenu() {  //Metode que retorna un valor enter
		int op;
		System.out.println(" Calculadora ");
		System.out.println("	0 Exit");
		System.out.println("	1 Obtenir dos numeros");
		System.out.println("	2 Sumar");
		System.out.println("	3 Restar ");
		System.out.println("	4 Multiplicar");
		System.out.println("	5 Divisi� ");
		System.out.println("	6 Visualitzar");
		System.out.println(" ");
	    op = reader.nextInt();
		return op;       //Retorna el valor al metode que li ha cridat
	} 
	 public static int Suma (int numero1, int numero2) { //metode que suma els numeros donats al programa
		
	        return numero1+numero2;
	    }
	 public static int Resta (int numero1, int numero2) { //metode que resta els numeros donats al programa
			
	        return numero1-numero2;
	    }
	 public static int Multiplicar (int numero1, int numero2) { //metode que multiplica els numeros donats al programa
			
	        return numero1*numero2;
	    } 
	 public static int Division (int numero1, int numero2) { //metodo para dividir los numeros 
			int opcion, resultado=0;
	        System.out.println("Division entera o Modulo?");
	        System.out.println("Si quieres la division entera, pulsa 1, para el modulo, pulse 2");
	        opcion=reader.nextInt();
	        
	        if (opcion==1) {
	        	resultado=DivisionEntera(numero1,numero2);
	        	
	        }
	        if (opcion==2) {
	        	resultado=Modulo(numero1, numero2);
	        }
	        
	        if (opcion!=1&&opcion!=2) {
	        	System.out.println("Error, opcion incorrecta");
	        }
	        
	        return resultado;
	        
	    }				
					public static int DivisionEntera(int numero1, int numero2) { //metodo para la division entera
						
						return numero1/numero2;
					}				
					public static int Modulo(int numero1, int numero2) { //metodo para el modulo
						
						return numero1%numero2;
					} 
						 
	
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
	 
	
}
